 <?php
if(isset($_POST['email'])) {
 
    $email_to = "dghais.ahmed7@gmail.com";
    $email_subject = "Contact our company";
    $messg = "";
    function died($error) {       
        $GLOBALS['messg'].='We are very sorry, but there were error(s) found with the form you submitted. These errors appear below.<br />'.$error.'<br /> Please go back and fix these errors.<br />';
    }
 
 
   
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
        !isset($_POST['message'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
 
    
 

    $first_name = $_POST['name']; 
    $email_from = $_POST['email']; 
    $comments = $_POST['message']; 
 
    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
  }
 
 
  if(strlen($comments) < 2) {
    $error_message .= 'The Comments you entered do not appear to be valid.<br />';
  }
 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n"; 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
     
 
    $email_message .= "First Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Comments: ".clean_string($comments)."\n";
 
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
mail($email_to, $email_subject, $email_message, $headers);  
}
?>

<!DOCTYPE html>
 <html lang="en">
 <head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1" />
 <title>Thank you </title>
 <meta name="description" content="Web Development , Mobile applications development and desktop application development" />
 <meta name="author" content="Dghais Ahmed" />

 <link rel="shortcut icon" href="img/logo.png"> 
 <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
 <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css" />

 <link rel="stylesheet" type="text/css" href="css/style.css" />
 <link rel="stylesheet" type="text/css" href="css/nivo-lightbox/nivo-lightbox.css" />
 <link rel="stylesheet" type="text/css" href="css/nivo-lightbox/default.css" />
    <style>
      a:hover, a:focus, a:active 
      {
        color: red !important;
      }
      
      body, html {
    height: 100%;
    margin: 0;
    background-image: url("img/ty.jpg");
    height: 100%; 
    background-position: center;
    background-repeat: no-repeat !important ;
    background-size: cover;
}
      </style>
       
 </head>
 <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

 <nav id="menu" class="navbar navbar-default navbar-fixed-top" style="height:60px;padding-top: 0px;margin-top:0%;">
   <div class="container" style="padding-top:0%;margin-top:0%;"> 
     <div class="navbar-header">
       <a class="navbar-brand page-scroll" href="#page-top" style="color: white;font-family :'Garamond','Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif ">PynToPyn </a> 
 </nav>
 <header >
   <div >
     <div class="overlay">
       <div class="container">
         <div class="row">
          <div class="intro-text" style="margin-top:0px;padding-top:10%;">
             <p style="color:#aaaaaa;background: rgba(43, 43, 43, 0.80);border-radius: 25px;font-size:30px;" id="para"> </p>
             <a href="./index.php#contact-h" style="font-size:18px;color:white;">Go back &gt;</a>  
             </div>
         </div>
       </div>
     </div>
   </div>
 </header>
  
<script type="text/javascript"> 
    var mymsg = "<?= $messg; ?>"; 
    console.debug(mymsg);

    var x =document.getElementById("para");
    if (mymsg.length>0)
    {
      x.innerHTML=mymsg;

    }    
  else 
   x.innerHTML="Thank you for Contacting us.<br> Our Developers will work on your request as soon as possible.";
 </script>
  


 </body>
 </html>
