<!DOCTYPE html>
 <html lang="en">
 <head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=no" />
 <title>PynToPyn </title>
 <meta name="description" content="Web Development , Mobile applications development and desktop application development" />
 <meta name="author" content="Dghais Ahmed" />


 <link rel="shortcut icon" href="img/logo.png"> 
 <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
 <link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css" />

 <link rel="stylesheet" type="text/css" href="css/style.css" />
 <link rel="stylesheet" type="text/css" href="css/nivo-lightbox/nivo-lightbox.css" />
 <link rel="stylesheet" type="text/css" href="css/nivo-lightbox/default.css" />
  <link rel="stylesheet" type="text/css" href="css/footer.css" />



  <!--[if lt IE 9]>
      <script src="../oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="../oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
      #readMore:hover
      {
        color:red !important;
      }
      a:hover, a:focus, a:active 
      {
        color: red !important;
      }
      hr
      {
        border-color:red !important;
        background-color: red !important;
      }
      #middle
      {
        display: inline-block;
        margin-left: 260px;
        margin-top: 25px;
        margin-bottom: 25px;
      }
       @media (max-width: 767px) {
   .collapse
      {
        background :#262626;
      }
      @media (max-width: 620px) {
  .intro h1 {
    font-size: 90px;
  }
}
}
      </style>
 </head>
 <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
 <nav id="menu" class="navbar navbar-default navbar-fixed-top" style="height:60px;padding-top: 0px;margin-top:0%;">
   <div class="container" style="padding-top:0%;margin-top:0%;"> 
     <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">  <span class="sr-only">Toggle navigation </span>  <span class="icon-bar"></span>  <span class="icon-bar"></span>  <span class="icon-bar"></span>  </button>
       <a class="navbar-brand page-scroll" href="#page-top" style="color: white;font-family :'Garamond','Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif ">PynToPyn </a> 
        </div>
    
     <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
       <ul class="nav navbar-nav navbar-right">
         <li><a href="#about" class="page-scroll"> About </a></li>
         <li><a href="#services" class="page-scroll"> Services </a></li>
         <li><a href="#portfolio" class="page-scroll">Our Work </a></li>
         <li><a href="#call-reservation" class="page-scroll">Contact </a></li>
       </ul>
     </div>
   </div>
 </nav>



 <header id="header">
   <div class="intro">
     <div class="overlay">
       <div class="container">
         <div class="row">
           <div class="intro-text" style="margin-top:0px;padding-top:18%;">
             <h1 style="padding-bottom:30px;">Be Unique </h1>
             <p style="color:#7f827c;">Web Development / Mobile Application Development / Desktop Application Development </p>
             <a href="#about" style="font-size:18px;color:white;" class="page-scroll">Know more &gt;</a>  </div>
         </div>
       </div>
     </div>
   </div>
 </header>
 <!-- About Section -->
 <div id="about">
   <div class="container">
     <div class="row">
       <div class="col-xs-12 col-md-6 ">
         <div class="about-img"><img src="img/about.jpg" class="img-responsive" alt="" /></div>
       </div>
       <div class="col-xs-12 col-md-6">
         <div class="about-text" style="text-align:center">
           <h2>Our Company </h2>
           <hr style="width:260px;margin:auto;"/>
           <div style="text-align:center;">
       <img src="img/logo3.png" name="logo" alt="" />
       </div>
           <p>
           PynToPyn is a new company specialized in applications and web development composed of a group of creative Developers and designers working in harmony and aiming for our client satisfaction

           </p>
           <a href="about.html"> <p style="text-align:center;color:black;" id="readMore">Read More &gt;</p></a>
           </div>
       </div>
     </div>
   </div>
 </div>
 <!-- Services Section -->
 <div id="services">
   <div class="section-title text-center center">
     <div class="overlay">
       <h2>Services </h2>
       <hr style="width:160px;color:red;"/>
       <div>
       <img src="img/hand-shake.png" name="logo" alt="" />
       </div>
       <p>take a look on our highly quality services </p>
     </div>
   </div>
   <div class="container">
     <div class="row">
     <div class="col-xs-12 col-md-4" style="margin:auto;">
         <div class="Services-section">
           <h2 class="Services-section-title">Desktop Application Development </h2>
           <hr style="width:290px;margin:auto;" />
            <div style="text-align:center;">
       <img src="img/laptop-b.png" name="logo" alt="" />
       </div>
         <div>
           <p style="margin-top:20px;">
            We offer platform-specific and cross-platform desktop app development services
              to help you turn a great business idea or any other awesome ideas into reality with help of our best developers
             </p>
           </div>
         </div>
       </div>
       <div class="col-xs-12 col-md-4">
         <div class="Services-section">
           <h2 class="Services-section-title" >Web Development</h2>
           <hr style="width:290px;margin:auto;"/>
           <div style="text-align:center;">
       <img src="img/web-b.png" name="logo" alt="" />
       </div>
          <div>
            <p style="margin-top:20px;"> Web development is a really rich domain to get into it . it could take many different forms and its evolving every day even the moment that you are reading this .<br>
             Our team of professional developers will insure that you will get the best website for your blogging ,business and many many other fields </p>
          </div>
         </div>
       </div>
       <div class="col-xs-12 col-md-4">
         <div class="Services-section">
           <h2 class="Services-section-title">Mobile Development </h2>
           <hr style="width:290px;margin:auto;" />
           <div style="text-align:center;margin-top:25px;">
       <img src="img/mobile-b.png" name="logo" alt="" />
       </div>
         <div>
           <p style="margin-top:20px;">
              we have the best qualified mobile developer that will be working with each other to make your dream application came to reality
for the time being we only develop android application.<br> soon we will take all platforms
             </p>
         </div>
         </div>
       </div>
     </div>
   </div>
   <a href="services.html" > <p style="text-align:center;color:black;" id="readMore">Read More &gt;</p> </a>
 </div>
 <!-- Our-Work Section -->
 <div id="portfolio" style="margin-bottom:0px;">
   <div class="section-title text-center center">
     <div class="overlay">
       <h2>Our work </h2>
       <hr style="width:160px"/>
       <div style="text-align:center;margin-bottom:18px;">
       <img src="img/work.png" name="logo" alt="" />
       </div>
       <p>these are some of our masterpieces </p>
     </div>
   </div>
   <div class="container">
     <div class="row">
       <div class="categories">
       </div>
     </div>
     <div class="row">
       <div class="portfolio-items">
         <div class="col-sm-6 col-md-4 col-lg-4 ">
           <div class="portfolio-item">
             <div class="hover-bg">  <a href="img/work/01-large.jpg" title="Facebook" data-lightbox-gallery="gallery1">
               <div class="hover-text">
                 <h4>Facebook </h4>
               </div>
               <img src="img/work/01-small.jpg" class="img-responsive" alt="Project Title" />  </a>  </div>
           </div>
         </div>

         <div class="col-sm-6 col-md-4 col-lg-4 ">
           <div class="portfolio-item">
             <div class="hover-bg">  <a href="img/work/02-large.jpg" title="youtube" data-lightbox-gallery="gallery1">
               <div class="hover-text">
                 <h4>Youtube</h4>
               </div>
               <img src="img/work/02-small.jpg" class="img-responsive" alt="Project Title" />  </a>  </div>
           </div>
         </div>

         <div class="col-sm-6 col-md-4 col-lg-4 ">
           <div class="portfolio-item">
             <div class="hover-bg">  <a href="img/work/03-large.jpg" title="Google" data-lightbox-gallery="gallery1">
               <div class="hover-text">
                 <h4>Google </h4>
               </div>
               <img src="img/work/03-small.jpg" class="img-responsive" alt="Project Title" />  </a>  </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <a href="work.html" > <p style="text-align:center;padding-bottom:0px;margin-bottom:0px;margin-top:30px; color:black;" id="readMore">Read More &gt;</p> </a>
 </div>
 <!-- Call Reservation Section -->
 <div id="call-reservation" class="text-center">
   <div class="container">
      <div style="text-align:center;margin-bottom:35px;">
       <img src="img/phone.png" name="logo" alt="" />
       </div>
     <h2>Want to be unique? Call  <strong>+216 24 012 144 </strong></h2>
   </div>
 </div>
 <!-- Contact Section -->
 <div id="contact" class="text-center">
   <div class="container">
     <div class="section-title text-center">
       <h2 id="contact-h">Contact Form </h2>
       <hr style="width:235px;"/>
       <div style="text-align:center;margin-bottom:20px;">
       <img src="img/contact.png" name="logo" alt="" />
       </div>
       <p>feel free to ask what ever you want. </p>
     </div>
     <div class="col-md-10 col-md-offset-1">

       <form name="sentMessage" id="contactForm" novalidate="" action="try.php" method="post">
         <div class="row">
           <div class="col-md-6">
             <div class="form-group">
               <input type="text" id="name" name="name" class="form-control" placeholder="Name" required/>
               <p class="help-block text-danger" />
             </div>
           </div>
           <div class="col-md-6">
             <div class="form-group">
               <input type="email" id="email" name="email" class="form-control" placeholder="Email" required/>
               <p class="help-block text-danger" />
             </div>
           </div>
         </div>
         <div class="form-group">
           <textarea name="message" id="message" class="form-control" rows="4" placeholder="Message" required></textarea>
           <p class="help-block text-danger" />
         </div>
         <button type="submit" class="btn btn-custom btn-lg" >Send Message </button>
       </form>

     </div>
   </div>
 </div>
 <footer>
 <div id="footer">
   <div class="up-footer">
			<div class="container">
				<div class="row">
					<aside id="text-2" class="footer-widget widget_text" style="float:right;margin-right:30px;">			
						<div class="textwidget"><h1 style="color:white;" >we are UNIQUE <span>.</span></h1>
							<span>pyn2pyn@gmail.com</span>
							<span>+216 24 012 144</span>
						</div>
					</aside>
						<aside id="text-3" class="footer-widget widget_text" style="margin-left:30px;">
							<h2 class="sb-title" style="color:white;" >We are social</h2>			
								<div class="textwidget">
									<div class="social-widget">
										<ul class="social-icons">
											<li><a class="facebook" href="https://www.facebook.com/PynToPyn/"><i class="fa fa-facebook"></i></a></li>
											<li><a class="twitter" href="https://twitter.com/pyn2pyn"><i class="fa fa-twitter"></i></a></li>
											<li><a class="google" href="https://plus.google.com/101591794765377716364"><i class="fa fa-google-plus"></i></a></li>
											<li><a class="linkedin" href="https://www.linkedin.com/in/pyntopyn-technologies-577352148/"><i class="fa fa-linkedin"></i></a></li>
											<li><a class="pinterest" href="https://www.pinterest.com/pyn2pyn/"><i class="fa fa-pinterest"></i></a></li>
										</ul>
									</div>
								</div>
						</aside>		
				</div>
			</div>
		</div>
      <div class="container-fluid text-center copyrights">
        <div class="col-md-8 col-md-offset-2">
        <p>&copy; 2017 PynToPyn. All right reserved. Designed by PynToPyn </p>
        </div>
      </div>
  </div>
 </footer>
 <script type="text/javascript" src="js/jquery.1.11.1.js"></script> 
 <script type="text/javascript" src="js/bootstrap.js"></script> 
 <script type="text/javascript" src="js/SmoothScroll.js"></script> 
 <script type="text/javascript" src="js/nivo-lightbox.js"></script> 
 <script type="text/javascript" src="js/jquery.isotope.js"></script> 
 <script type="text/javascript" src="js/main.js"></script>
 

 </body>
 </html>
 